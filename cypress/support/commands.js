import { urlSites } from "../utils/urlSites";
import { clickTableSetup } from "../utils/global";
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.

// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
Cypress.Commands.add('login', (time, email = "administrador@gmail.com", password = "12345678") => {
  cy.visit(urlSites.Avanzza);
  cy.wait(time);

  cy.get('input[id="email"]').should("be.visible").type(email);
  cy.get('input[id="password"]').should("be.visible").type(password);

  cy.get("button[type=submit]")
    .should("have.text", "Iniciar sesión")
    .should("be.visible")
    .should("be.enabled")
    .click();
  cy.wait(time);
})

Cypress.Commands.add('filterData', (attributes, shouldClose = true) => {
  cy.get('div[data-cy="button-filter"]')
    .should('be.visible')
    .click();
  attributes.forEach((attribute) => {
    let type = attribute.type
    let name = attribute.name
    let value = attribute.value
    let input = `${type}[data-cy="input-${name}"]`

    type === 'select' ?
      cy.get(input).should('be.visible').select(value) :
      cy.get(input).should('be.visible').type(value)
  })
  cy.contains('button', /filtrar|filter/i)
    .should('be.visible')
    .click();

  if (shouldClose) {
    attributes.forEach((attribute) => {
      let type = attribute.type
      let name = attribute.name
      let input = `${type}[data-cy="input-${name}"]`

      type === 'select' ?
        cy.get(input).select('Seleccione') :
        cy.get(input).clear()
      cy.wait(500)
    })
    cy.contains('button', /filtrar|filter/i)
      .should('be.visible')
      .click();

    cy.get('#filter-form [aria-label="Close"]')
      .should('be.visible')
      .click()
  }

})

Cypress.Commands.add('testTableSetup', (entityName) => {
  const checkboxGroup = 'fieldset.form-group input[type="checkbox"]'

  clickTableSetup()

  cy.get(checkboxGroup).uncheck({ force: true })

  cy.get(checkboxGroup).each(($element, index, $list) => {
    cy.intercept('GET', `/api/custom-table/${entityName}`).as(`customTable${index}`)
    let defaultValue = Cypress.$($element)[0].defaultValue
    cy.get(checkboxGroup).eq(index).check({ force: true });
    cy.get('footer.modal-footer button[type="button"]')
      .contains('OK')
      .should('be.visible')
      .click();    
    cy.wait(`@customTable${index}`).should(interception => {
      expect(interception.response.statusCode).to.eq(200)
      expect(interception.response.body[index].name).to.equal(defaultValue);
    })
    if (index + 1 !== $list.length) {
      clickTableSetup()
    }
  })
})
Cypress.Commands.add('selectTab',(position)=>{
  let selector = `ul[role="tablist"] li.nav-item a[aria-posinset="${position}"]`
  cy.get(selector).eq(1).click({ force: true });
})

Cypress.Commands.add('deleteElement',(elements, entityName)=>{
  let lastElement =  elements.length -1. 

  elements.forEach((codigo, index) => {
    cy.get('[data-cy=data-table-wrapper] table tbody tr')
      .contains('td', codigo)
      .parent()
      .find('[data-cy=table-button-delete]')
      .click();
    if(lastElement == index){
      cy.intercept('DELETE', `/api/${entityName}/*`).as(`delete${entityName}`)
    }  
    cy.get('button.swal2-confirm')
      .should("be.visible")
      .click();

  });
});
