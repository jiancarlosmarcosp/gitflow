import { faker } from '@faker-js/faker';

const importedCustomerCodes = ["IMPORTCLI01","IMPORTCLI02","IMPORTCLI03"]

export const Customer = {
  code: faker.random.alphaNumeric(6),
  name: faker.name.fullName(),
  typeDocument: ["dni","ruc"],
  document: faker.datatype.number({ min: 10000000, max: 99999999 }),
  entityName:'clients'
};

export const deleteCustomerByCode= (codigos=importedCustomerCodes) => {
  codigos.forEach((codigo) => {
    cy.get('[data-cy=data-table-wrapper] table tbody tr')
      .contains('td', codigo)
      .parent()
      .find('[data-cy=table-button-delete]')
      .click();
    cy.get('button.swal2-confirm')
      .should("be.visible")
      .click();
  });
};