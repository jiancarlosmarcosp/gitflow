export const randomEmail = (numberCharacters = 8) => {
  const listCharacters = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0",
  ];

  let wordGenerate = "";
  for (let i = 0; i < numberCharacters; i++) {
    const indexObtain = parseInt(
      Math.round(Math.random() * (listCharacters.length - 1))
    );
    const letterRandom = listCharacters[indexObtain];
    wordGenerate = `${wordGenerate}${letterRandom}`;
  }

  return `${wordGenerate}@mailinator.com`;
};
