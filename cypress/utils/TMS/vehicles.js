import { faker } from '@faker-js/faker';

export const importedVehicleCodes = ["PLC745","PLC746","PLC747","PLC748"]

export const Vehicle = {
    codeProvider: "cyProviderName",
    codeDriver: "cyDriverName",
    vehicle_placa: faker.random.alphaNumeric(6),
    vehicle_placa_edit: "BBB963",
    vehicle_motor: "SI",
    vehicle_carga: "SI",
    num_ejes: faker.random.numeric(2),
    maximo_volumen: faker.random.numeric(2),
    maximo_peso: faker.random.numeric(2),
    num_llantas: "4",
    vehicle_estado: "Disponible",
    vehicle_type: "Minibus",
    operation: "OPE001|OPE001",
    entityName:'vehicles'
};