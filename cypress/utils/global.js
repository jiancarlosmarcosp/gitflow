export const clickTableSetup = () => {
  cy.get('button.dropdown-toggle.btn-primary')
    .should('be.visible')
    .click();
  cy.get('ul.dropdown-menu[x-placement="bottom-start"]')
    .should('be.visible')
    .click();
}