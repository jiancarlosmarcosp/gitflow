Feature: Flujos clientes

Scenario: Inicio
    Given Inicio sesión
    And Ingreso al módulo Clientes

Scenario: Registro de un nuevo cliente
    When hago clic en el botón Nuevo Cliente
    And lleno los campos de código, nombre, tipo de documento y número de documento
    And hago clic en el botón Guardar
    Then se guarda el nuevo cliente correctamente    

Scenario: Edición de un cliente existente
    When hago clic en el botón Editar
    And realizo las modificaciones necesarias en los campos de código, nombre, tipo de documento y número de documento
    And hago clic en el botón Guardar Cambios
    Then se guardan los cambios correctamente

Scenario: Buscar cliente por nombre
    When Escribo el nombre del cliente que deseo buscar
    Then se muestra correctamente el resultado de la busqueda

#Scenario: Importación masiva de clientes
#    When hago clic en el botón Importar
#    And selecciono el archivo e importo
#    And los clientes son importados correctamente

Scenario: Filtrar los atributos del cliente uno por uno
    When Filtro por Código
    And Filtro por Nombre
    And Filtro por Tipo de Documento
    And Filtro por Número de Documento

Scenario: Eliminación de un cliente existente
    When hago clic en el botón Eliminar
    And confirmo la eliminación
    Then el cliente es eliminado correctamente

Scenario: Exportación de clientes
    When hago clic en el botón Exportar
    Then se descarga correctamente un archivo con la información de los clientes

Scenario: Crear Tarifas
   When Click en el botón crear tarifa
   When Click en el botón crear nueva tarifa