import { urlSites } from "../../../../utils/urlSites";
import { Customer, deleteCustomerByCode } from "../../../../utils/CRM/clients";

const time = 2500;

Cypress.on("uncaught:exception", (err, runnable) => {
  return false
});

Given('Inicio sesión',()=>{
  cy.login(time)
});

And("Ingreso al módulo Clientes", () => {
  cy.visit(urlSites.Avanzza + `/${Customer.entityName}`)
});

// //Registro de un nuevo cliente
When("hago clic en el botón Nuevo Cliente", () => {
  cy.get('button[data-cy="button-create"]').click();
});

And('lleno los campos de código, nombre, tipo de documento y número de documento',()=>{
  cy.get('input[data-cy="input-code"]')
    .should('be.visible')
    .should('have.class', 'form-control')
    .type(Customer.code);

  cy.get('input[data-cy="input-name"]')
    .should('be.visible')
    .should('have.class', 'form-control')
    .type(Customer.name);

  cy.get('select[data-cy="input-document_type_code"]')
    .should('be.visible')
    .select(Customer.typeDocument[0]);

  cy.get('input[data-cy="input-document_number"]')
    .should('be.visible')
    .should('have.class', 'form-control')
    .type(Customer.document);
});

And("hago clic en el botón Guardar", () => {
  cy.intercept('POST', `/api/${Customer.entityName}`).as('createClient')
  cy.get('button[data-cy="crud-save-button"]')
    .should("be.visible")
    .click();
});

Then('se guarda el nuevo cliente correctamente',()=>{
  cy.wait('@createClient').should(interception => {
    expect(interception.response.statusCode).to.eq(201)
  })
  cy.contains('Creación correcta').should('be.visible');
})

//Edición de un cliente existente
When("hago clic en el botón Editar", () => {
  cy.get('button[data-cy="table-button-update"]:first').click();
});

When("Editar el código", () => {
    cy.get('input[data-cy="input-code"]')
      .should('be.visible')
      .should('have.class', 'form-control')
      .clear()
      .type(Customer.code)
      cy.wait(500);
  })

And('realizo las modificaciones necesarias en los campos de código, nombre, tipo de documento y número de documento',()=>{
  cy.get('input[data-cy="input-code"]', { timeout: 1500 })
    .should('be.visible')
    .clear()
    .type(Customer.code)     

  cy.get('input[data-cy="input-name"]', { timeout: 1500 })
    .should('be.visible')
    .clear()
    .type("Cliente a buscar");

  cy.get('select[data-cy="input-document_type_code"]', { timeout: 1500 })
    .should('be.visible')
    .select(Customer.typeDocument[1]);

  cy.get('input[data-cy="input-document_number"]', { timeout: 5000 })
    .should('be.visible')
    .clear()
    .type(Customer.document);
});

And("hago clic en el botón Guardar Cambios", () => {
  cy.intercept('PATCH', `/api/${Customer.entityName}/*`).as('editClient')
  cy.get('button[data-cy="crud-save-button"]')
    .click();
});

Then('se guardan los cambios correctamente',()=>{
  cy.wait('@editClient').should(interception => {
    expect(interception.response.statusCode).to.eq(200)
  })
  cy.contains('Actualizado correctamente.').should('be.visible');
});

//Buscar cliente por nombre
When('Escribo el nombre del cliente que deseo buscar',()=>{
  cy.get('input[type="search"]')
    .type("Cliente a buscar")
});

Then('se muestra correctamente el resultado de la busqueda', () => {
  cy.intercept('GET', '/api/clients**').as('searchClient');
  cy.wait('@searchClient').then(({ response }) => {
    if (response.body.data.length === 0) {
      throw new Error('No se encontraron resultados de búsqueda');
    } else {
      expect(response.body.data[0].name).to.equal('Cliente a buscar');
    }
  })
});

// Importación masiva de clientes
When('hago clic en el botón Importar',()=>{
  cy.get(`a[href="/imports/${Customer.entityName}"]`).click();
});
When("selecciono el archivo e importo", () => {
  cy.fixture('import_client.xlsx','binary')
    .then(Cypress.Blob.binaryStringToBlob)
    .then(fileContent => {
      cy.get('input[type="file"]').attachFile({
        fileContent,
        fileName: 'import_client.xlsx', 
        mimeType: 'application/vnd.ms-excel'
      })
  })
});

And('los clientes son importados correctamente', () => {

  cy.intercept('POST', `/api/imports/${Customer.entityName}/run-import`).as('importClients')
  cy.wait(1500);
  cy.get('button.wizard-footer-right').click();
  cy.wait(1500);
  cy.get('button.wizard-btn').eq(1).click();

  cy.wait('@importClients').should(interception => {
    expect(interception.response.body.message).to.eq("La operacion fue exitosa")
    expect(interception.response.body.errors).to.have.length(0)
    expect(interception.response.body.stats.creates).to.eq(3)
  })
});

//Filtrar los atributos del cliente uno por uno
When('Filtro por Código',()=>{
  cy.filterData([
    { type: 'input', name: 'code', value: 'IMPORTCLI01' }
  ])
  cy.wait(1500)
});
And('Filtro por Nombre', ()=>{
  cy.filterData([
    { type: 'input', name: 'name', value: 'Molitalia SA' }
  ])
  cy.wait(1500)
});
And('Filtro por Tipo de Documento', ()=>{
  cy.filterData([
    { type: 'select', name: 'document_type_code', value: 'ruc' }                             
  ])
  cy.wait(1500)
});
And('Filtro por Número de Documento', ()=>{
  cy.filterData([
    { type: 'input', name: 'document_number', value: '78865421' }
  ])
  cy.wait(1500)
});

// Eliminación de un cliente existente
When("hago clic en el botón Eliminar", () => {
  deleteCustomerByCode()
  cy.get('button[data-cy="table-button-delete"]:first').click();
});

And("confirmo la eliminación", () => {
  cy.intercept('DELETE', `/api/${Customer.entityName}/*`).as('deleteClient')
  cy.get('button.swal2-confirm')
    .should("be.visible")
    .click();
});

Then('el cliente es eliminado correctamente',()=>{
  cy.wait('@deleteClient').should(interception => {
    expect(interception.response.statusCode).to.eq(204)
  })
  cy.contains('Se ha eliminado correctamente.').should('be.visible');
});

// Then('Limpio los registros para las nuevas pruebas',()=>{
//   cy.wait(1500);
//   deleteCustomerByCode()
// })

// Exportar Clientes
When("hago clic en el botón Exportar", () => {
  cy.get('button[data-cy="button-download"]').click();
  cy.wait(time);
});

Then("se descarga correctamente un archivo con la información de los clientes", ()=>{
  cy.task('readDirectory', 'cypress/downloads').then((files) => {
    const downloadedFile = files.find((file) => file.includes('client_report'));
    expect(downloadedFile).to.exist;

    if (downloadedFile) {
      cy.task('deleteFile', `cypress/downloads/${downloadedFile}`);
    }
  });
})

// // Crear Tarifas
// // When("Click en el botón crear tarifa", () => {
// //   cy.get('button[data-cy="table-button-tariff"]').eq(0).click();
// // });

// // When("Click en el botón crear nueva tarifa", () => {
// //   cy.get('button[data-cy="button-create"]').eq(0).click();
// // });